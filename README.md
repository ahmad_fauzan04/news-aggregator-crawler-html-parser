# README #
News agregator bahasa Indonesia. Ide diambil dari news.google.com. Berita diambil, diklasifikasikan dalam kategori (politik/ekonomi dst), selanjutnya dikelompokkan (di-cluster) berdasarkan topik yang sama (ekonomi: pilpres, DPR).

Perbedaan dengan google news adalah adanya rangkuman setiap cluster yang diperoleh dengan ekstraksi otomatis komponen 5w1h (What, Where, Who, When, Why, How). Rangkuman ini akan disesuaikan dengan artikel yang sudah dibaca user (lebih menonjolkan informasi yang belum dibaca user). Artinya setiap user bisa mendapat rangkuman yang berbeda satu dengan lain.

Didanai oleh riset ITB 2014

### requirement ###
1. Java 1.6+
2. Mysql
3. Web server + PHP
 


### instalasi backend ###
Backend dan frontend terhubung melalui database.


Instalasi backend:
1. Buat db, ekstrak struktur database dari file:db/xxx.sql.zip   
2. Build .jar dari project netbeans  (petunjuk ada dibawah)
3. Gabungkan isi folder dist/ (.jar dan lib) dengan folder/file berikut ini.

* config/
* db/Database.conf
* db/DatabaseUtils.props
* models/
* resource/

4. Setting akses database pada file db/Database.conf dan db/DatabaseUtils.props

* Pada db/Database.conf, ubah nilai Host, Port, Username, Password dan Database (nama database).
* Pada db/DatabaseUtils.props, ubah bagian  jdbcURL=jdbc:mysql://localhost:3306/newsaggregator menjadi jdbcURL=jdbc:mysql://<host>:<port>/<db_name>. <host>,<port> dan <db_name> menyesuaikan dengan target koneksi

6. Jika menggunakan proxy, setting akses proxy untuk crawler pada file config/Proxy.conf

5. Jalankan .jar  

   - untuk menjalankan di background dan tetap running walaupun logoff: 
   
   nohup java -jar xxx.jar &
   
   - untuk menjalankan di background tanpa output ke file nohup
   nohup java -jar xxx.jar > /dev/null &
   
6. Setting via db atau frontend: tabel config.status 

## Langkah-langkah update model ##
tbd







### arsitektur sistem ###

Komponen utama:
Backend (java):
- Crawler    : mengumpulkan berita 
- Classifier : mengelompokan berita ke salah satu dari 10 kategori
- Clusterer  : mengelompokkan berita dengan topik sama (yg berada di dalam satu kategori)
- 5w1H_ekstraktor: mengekstrak informasi 5w1h dalam satu artikel
- Summarizer : memberikan rangkuman cluster berdasarkan data 5w1h dan artikel apa yg sudah dibaca user.

Frontend (php).
[tbd]
=======

### kontak ###
Ketua tim riset: Masayu L. Khodra (masayu@informatika.org)
Anggota: Ahmad Fauzan, Bagus, Yudi W
=======