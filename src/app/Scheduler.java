/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import clustering.NewsClustering;
import clustering.TextClustering;
import crawler.Crawler;
import crawler.TextAnalyzer;
import database.DatabaseManager;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ahmad
 */
public class Scheduler {
    long crawlerDelayTime = 1000; // in milliseconds
    long clusterDelayTime = 1000; // in milliseconds
    Thread crawlerThread = null;
    Thread clusterThread = null;
    Thread textAnalizerThread = null;
    boolean isRunning = true;
    Object syncObject = new Object();
    
    // Use Objects
    Crawler crawler = new Crawler();
    TextAnalyzer textAnalyzer = new TextAnalyzer();
    
    
    public Scheduler() {
        crawlerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                DatabaseManager.Log("Status", "Started");
                int idx = 0;
                while(true) {
                    // Check status
                    // If stop : continue and delay every 10 second
                    Hashtable<String, Integer> config = DatabaseManager.GetConfig();
                    if(isRunning == false) {
                        if(config.get("status").intValue() == 1) {
                            DatabaseManager.Log("Status", "Started");
                            isRunning = true;
                            continue;
                        }
                        
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        continue;
                        
                    } else if(config.get("status").intValue() != 1) {
                        isRunning = false;
                        DatabaseManager.Log("Status", "Stopped");
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        continue;
                    }
                    
                    // Check delay in database
                    crawlerDelayTime = config.get("durasi_parser").intValue()*1000;
                    long startTime = System.currentTimeMillis();
                        
                    synchronized(syncObject) {
                        System.out.println("Crawling...");
                        crawler.startCrawl(idx++);
                        if(idx >= crawler.feedSize()) {
                            idx = 0;
                        }
                    }
                    
                    long endTime = System.currentTimeMillis();
                    System.out.println("Delay...");
                    try {
                        Thread.sleep(crawlerDelayTime);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        
        textAnalizerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    // Check status
                    // If stop : continue and delay every 10 second
                    Hashtable<String, Integer> config = DatabaseManager.GetConfig();
                    if(isRunning == false) {
                        if(config.get("status").intValue() == 1) {
                            DatabaseManager.Log("Status", "Started");
                            isRunning = true;
                            continue;
                        }
                        
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        continue;
                        
                    } else if(config.get("status").intValue() != 1) {
                        isRunning = false;
                        DatabaseManager.Log("Status", "Stopped");
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        continue;
                    }
                    
                    // Check delay in database
                    crawlerDelayTime = config.get("durasi_parser").intValue()*1000;
                    long startTime = System.currentTimeMillis();
                        
                    synchronized(syncObject) {
                        System.out.println("Text Analyzing...");
                        textAnalyzer.textAnalysis();
                    }
                    
                    long endTime = System.currentTimeMillis();
                    System.out.println("Delay...");
                    if(endTime-startTime < crawlerDelayTime) {
                        try {
                            Thread.sleep(crawlerDelayTime - (endTime-startTime));
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        try {
                            Thread.sleep(crawlerDelayTime/2);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        });
        
        clusterThread = new Thread(new Runnable() {

            @Override
            public void run() {
                while(true) {
                    // Check status
                    // If stop : continue and delay every 10 second 
                    Hashtable<String, Integer> config = DatabaseManager.GetConfig();
                    if(isRunning == false) {
                        if(config.get("status").intValue() == 1) {
                            DatabaseManager.Log("Status", "Started");
                            isRunning = true;
                            continue;
                        }
                        
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        continue;
                        
                    } else if(config.get("status").intValue() != 1) {
                        isRunning = false;
                        DatabaseManager.Log("Status", "Stopped");
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        continue;
                    }
                    
                    // Check delay in database
                    clusterDelayTime = config.get("durasi_cluster").intValue()*1000;
                    
                    try {
                        Thread.sleep(clusterDelayTime);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                  
                    synchronized(syncObject) {
                        System.out.println("Clustering...");
                        NewsClustering.main(null);
                    }
                    
                }
            }
        });
    }
    
    public void start() {
        crawlerThread.start();
        textAnalizerThread.start();
        clusterThread.start();
        
    }
    
    public static void main(String[] args) throws SQLException {
        Scheduler scheduler = new Scheduler();
        scheduler.start();
    }
}
