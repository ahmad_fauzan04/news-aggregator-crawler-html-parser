/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crawler;

import classification.TextCategorizer;
import database.DatabaseManager;
import html_parser.DetikParser;
import html_parser.OkezoneParser;
import html_parser.TempoParser;
import html_parser.VivanewsParser;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.Authenticator;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import structure.News;
import connection.ProxyAuthenticator;
import html_parser.JPNNParser;
import informationextraction.Extractor;
import informationextraction.Filler;
import util.FileConfig;

/**
 *
 * @author Ahmad
 */
public class Crawler {
    
    String feedUrl[] = {"http://www.jpnn.com/index.php?mib=rss&id_subrubrik=62"};//"http://www.inilah.com/rss/feed/fokus/","http://sindikasi.okezone.com/index.php/rss/0/RSS2.0","http://rss.vivanews.com/get/politik","http://rss.detik.com/index.php/detikcom" ,"http://www.tempo.co/rss/terkini", "http://www.tempo.co/rss/bisnis"};
    HashMap<String, HTMLParser> parsers;
    ArrayList<News> results;
    FileConfig proxyConfig;

    /**
     * Constructor Crawler
     */
    public Crawler() {
        
        // Register Parser
        parsers = new HashMap<>();
        parsers.put("www.tempo.co", new TempoParser());
        parsers.put("tempo.co", new TempoParser());
        parsers.put("rss.detik.com", new DetikParser());
        parsers.put("rss.vivanews.com", new VivanewsParser());
        parsers.put("sindikasi.okezone.com", new OkezoneParser());
        parsers.put("www.jpnn.com", new JPNNParser());
        results = new ArrayList<>();
        
       
        
        // Load news source from database
        feedUrl = DatabaseManager.getNewsSource();
        
        // ProxyConfig
        proxyConfig = new FileConfig();
        proxyConfig.loadConfig("config/Proxy.conf");
        if(proxyConfig.getConfigValue().containsKey("Proxy") &&
                proxyConfig.getConfigValue().get("Proxy").compareTo("true") == 0) {
            if(proxyConfig.getConfigValue().get("Authentication").compareToIgnoreCase("true") == 0) {
                this.setProxy(proxyConfig.getConfigValue().get("Host"),
                        proxyConfig.getConfigValue().get("Port"), 
                        proxyConfig.getConfigValue().get("Username"), 
                        proxyConfig.getConfigValue().get("Password"));
            } else {
                this.setProxy(proxyConfig.getConfigValue().get("Host"),
                        proxyConfig.getConfigValue().get("Port"));
            }
        }
        
    }
    
    /**
     * Start Crawler from feed
     */
    public void startCrawl() {
        results.clear();
        for(int i=0; i < feedUrl.length; i++) {
            readURL(feedUrl[i]);
            DatabaseManager.SaveNews(results.toArray(new News[results.size()]));
            results.clear();
        }
    }
    
    /****
     * Crawl per index of feed
     * @param index index of feed
     */
    public void startCrawl(int index) {
        //results.clear();
        DatabaseManager.Log("crawl", "crawling from " + feedUrl[index]);
        readURL(feedUrl[index]);
        
        DatabaseManager.SaveNews(results.toArray(new News[results.size()]));
        DatabaseManager.Log("crawl", "crawled "+ results.size() +" news from " + feedUrl[index]);
        results.clear();
    }
    
    /***
     * Get Size of feed
     * @return number of feed
     */
    public int feedSize() {
        return feedUrl.length;
    }
    
    public void readURL(String urlString) {
        try {
            
            final URL url = new URL(urlString);
            Reader reader = new InputStreamReader(url.openStream(),"UTF-8");
                    
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");
            
            Document doc = dBuilder.parse(is);
         
            doc.getDocumentElement().normalize();
            
            System.out.println("Root element : " + doc.getDocumentElement().getNodeName());
            
            NodeList nList = doc.getElementsByTagName("item");
            
            
            for(int i=0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                
                Element e = (Element) node;
                
                News news = new News();
                // Read title
                String title = e.getElementsByTagName("title").item(0).getTextContent();
                news.setTitle(title);
                
                // Read public Date
                String pubDate = e.getElementsByTagName("pubDate").item(0).getTextContent();
                news.setPubDate(pubDate);
                
                // Read Link
                String link = e.getElementsByTagName("link").item(0).getTextContent();
                news.setUrl(link);
                
                
                if(DatabaseManager.isNewsExist(news)){ 
                    DatabaseManager.Log("parser", "news already exist");
                    continue;
                }
                
                // Parse to get content and image
                String content = "";
                String image = "";
                String site = urlString.split("/")[2];
                //System.out.println(site);
                if(parsers.containsKey(site)) {
                    HTMLParser parser = parsers.get(site);
                    if(!parser.parse(link) || 
                            parser.getTextContent().compareTo("") == 0) {
                        DatabaseManager.Log("parser", "parser cannot load from : " + link);    
                        continue;
                    }
                    content = parser.getTextContent();
                    image = parser.getImage();
                } else {
                    DatabaseManager.Log("parser", "parser not found");
                    continue;
                }
                
                System.out.println(content + "\n" + image);
                
                news.setContent(content);
                news.setImageLink(image);
                
                results.add(news);
            }   
        } catch (Exception ex) {
            Logger.getLogger(Crawler.class.getName()).log(Level.SEVERE, null, ex);
            DatabaseManager.Log("crawl", "["+ urlString +"] Error : " + ex.getMessage());
        } 
        
    }
    
    
    
    public ArrayList<News> getResults() {
        return results;
    }
    
    public void setProxy(String host, String port, String username, String password) {
        Authenticator.setDefault(new ProxyAuthenticator(username, password));
        System.setProperty("http.proxyHost", host);
        System.setProperty("http.proxyPort", port);
    }
    
     public void setProxy(String host, String port) {
        System.setProperty("http.proxyHost", host);
        System.setProperty("http.proxyPort", port);
    }
}
