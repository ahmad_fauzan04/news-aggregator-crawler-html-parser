/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crawler;

import classification.TextCategorizer;
import database.DatabaseManager;
import informationextraction.Extractor;
import informationextraction.Filler;
import java.util.ArrayList;
import structure.News;

/**
 *
 * @author Ahmad
 */
public class TextAnalyzer {
    
    
    TextCategorizer tc;
    Extractor extractor;

    public TextAnalyzer() {
         // Load Text Categorizer Model
        tc = new TextCategorizer("models/kategorisasi/NBfiltered_structure_alpha_low.mod");
        
        // Load Model for Extractor
        //I:\\Riset\\2014\\news_aggregator\\5W1H\\j48_30_191114.mod
        extractor = new Extractor("models/j48_30_191114.mod");
    }
    
    
    public void textAnalysis() {
        
        ArrayList<News> unprocessNews = DatabaseManager.GetUnprocessNews();
        System.out.println(unprocessNews.size());
        for (News news : unprocessNews) {
            // Set Kategori
            news.setCategory(tc.predict(new String[]{"judul","full_text"}, 
                    new String[]{news.title,news.content}));

            // Extract 5W1H
            try {
                ArrayList<Filler> result5w1h=extractor.process(news.title,news.content);
                //System.out.println("Hasil ekstraksi 5W1H: ");
                for (int j=0;j<result5w1h.size();j++) {
                    //System.out.println(result5w1h.get(j).tag+" : "+result5w1h.get(j).text);
                    if (result5w1h.get(j).tag.equalsIgnoreCase("what"))
                        news.setWhat(result5w1h.get(j).text);
                    else if (result5w1h.get(j).tag.equalsIgnoreCase("when"))
                        news.setWhen(result5w1h.get(j).text);
                    else if (result5w1h.get(j).tag.equalsIgnoreCase("where"))
                        news.setWhere(result5w1h.get(j).text);
                    else if (result5w1h.get(j).tag.equalsIgnoreCase("who"))
                        news.setWho(result5w1h.get(j).text);
                    else if (result5w1h.get(j).tag.equalsIgnoreCase("why"))
                        news.setWhy(result5w1h.get(j).text);
                    else //if (result5w1h.get(j).tag.equalsIgnoreCase("how"))
                        news.setHow(result5w1h.get(j).text);

                }
            } catch(Exception e) {
                DatabaseManager.Log("TextAnalyzer", "Error Information Extraction, news with id = " + news.id);
            }
            /*HashMap<String,Filler> result5w1h = extractor.process(news.title,news.content);
            System.out.println("Hasil ekstraksi 5W1H: ");
            Iterator it = result5w1h.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry item = (Map.Entry) it.next();
                System.out.println(item.getKey() + " = " + ((Filler)item.getValue()).text.replaceAll("'",""));
            }

            if(result5w1h.containsKey("what")) {
                news.setWhat(result5w1h.get("what").text.replaceAll("'", ""));
            }

            if(result5w1h.containsKey("when")) {
                news.setWhen(result5w1h.get("when").text.replaceAll("'", ""));
            }

            if(result5w1h.containsKey("where")) {
                news.setWhere(result5w1h.get("where").text.replaceAll("'", ""));
            }

            if(result5w1h.containsKey("who")) {
                news.setWho(result5w1h.get("who").text.replaceAll("'", ""));
            }

            if(result5w1h.containsKey("how")) {
                news.setHow(result5w1h.get("how").text.replaceAll("'", ""));
            }*/
        }
        
        DatabaseManager.UpdateNews(unprocessNews);
    }
}
