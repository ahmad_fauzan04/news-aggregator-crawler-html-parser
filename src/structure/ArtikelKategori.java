/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package structure;

import database.DatabaseHelper;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Representasi entity set artikel_kategori.
 * @author Bagus Rahman Aryabima
 */
public class ArtikelKategori {
    /**
     * Menghitung jumlah kategori pada entity set artikel_kategori.
     * @return Jumlah kategori pada entity set artikel_kategori.
     */
    public static int countIDKelas() {
        int result = 0;
        
        String query = "SELECT COUNT(DISTINCT ID_KELAS) FROM artikel_kategori";
        ResultSet rs = DatabaseHelper.executeQuery(query);
        
        try {
            if (rs.first()) {
                result = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ArtikelKategori.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    public static ArrayList<Integer> selectIDKelas() {
        ArrayList<Integer> result = new ArrayList<>();
        
        String query = "SELECT DISTINCT ID_KELAS FROM artikel_kategori";
        ResultSet rs = DatabaseHelper.executeQuery(query);
        
        try {
            while (rs.next()) {
                result.add(rs.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ArtikelKategori.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
}
