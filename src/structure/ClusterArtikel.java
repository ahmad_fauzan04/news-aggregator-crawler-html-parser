/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package structure;

import database.DatabaseHelper;

/**
 * Representasi entity set cluster_artikel.
 * @author Bagus Rahman Aryabima
 */
public class ClusterArtikel {
    /**
     * Memasukkan entity cluster_artikel baru.
     * @param id_artikel Atribut id_cluster entity cluster_artikel baru.
     * @param id_cluster Atribut id_kelas entity cluster_artikel baru.
     */
    public static void insertClusterArtikel(String id_artikel, String id_cluster) {
        String query = "INSERT INTO cluster_artikel VALUES ('"
                + id_artikel + "', '"
                + id_cluster + "', "
                + "NULL, "
                + "NULL) "
                + "ON DUPLICATE KEY UPDATE "
                + "ID_CLUSTER=" + id_cluster + ", "
                + "ID_ARTIKEL=" + id_artikel;
        DatabaseHelper.execute(query);
    }
    
    /**
     * Menghapus seluruh entity pada tabel cluster_artikel
     */
    public static void deleteClusterArtikel() {
        String query = "DELETE FROM cluster_artikel";
        DatabaseHelper.execute(query);
    }
}
