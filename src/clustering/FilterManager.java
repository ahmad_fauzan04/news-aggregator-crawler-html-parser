/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clustering;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import weka.core.Instances;
import weka.core.tokenizers.WordTokenizer;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.NominalToString;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.StringToWordVector;

/**
 * Modul pendukung pemanfaatan filter Weka.
 * @author Bagus Rahman Aryabima
 */
public class FilterManager {
    /**
     * Menerapkan filter Remove pada Instances instances, tepatnya pada atribut ke-attributeIndex.
     * @param instances Instances yang difilter dengan filter Remove.
     * @param attributeIndex Nomor atribut diterapkannya filter Remove.
     * @return Instances hasil penerapan filter Remove pada Instances instances, tepatnya pada atribut ke-attributeIndex.
     */
    public static Instances Remove(Instances instances, int attributeIndex) {
        Instances result = null;
        
        try {
            Remove remove = new Remove();
            String[] options = weka.core.Utils.splitOptions("-R " + attributeIndex);
            remove.setOptions(options);
            remove.setInputFormat(instances);
            result = Filter.useFilter(instances, remove);
        } catch (Exception ex) {
            Logger.getLogger(FilterManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    /**
     * Menerapkan filter NominalToString pada Instances instances, tepatnya pada atribut ke-attributeIndex.
     * @param instances Instances yang difilter dengan filter NominalToString.
     * @param attributeIndex Nomor-nomor atribut diterapkannya filter NominalToString.
     * @return Instances hasil penerapan filter NominalToString pada Instances instances, tepatnya pada atribut ke-attributeIndex.
     */
    public static Instances NominalToString(Instances instances, ArrayList<Integer> attributeIndex) {
        Instances result = null;
        
        try {
            NominalToString nominalToString = new NominalToString();
            String[] options = weka.core.Utils.splitOptions("-C " + StringUtils.join(attributeIndex, ','));
            nominalToString.setOptions(options);
            nominalToString.setInputFormat(instances);
            result = Filter.useFilter(instances, nominalToString);
        } catch (Exception ex) {
            Logger.getLogger(FilterManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    /**
     * Menerapkan filter StringToWordVector pada Instances instances, tepatnya pada atribut ke-attributeIndex.
     * @param instances Instances yang difilter dengan filter StringToWordVector.
     * @param attributeIndex Nomor-nomor atribut diterapkannya filter StringToWordVector.
     * @return Instances hasil penerapan filter StringToWordVector pada Instances instances, tepatnya pada atribut ke-attributeIndex.
     */
    public static Instances StringToWordVector(Instances instances, ArrayList<Integer> attributeIndex) {
        Instances result = null;
        
        try {
            StringToWordVector stringToWordVector = new StringToWordVector();
            String[] options = weka.core.Utils.splitOptions("-R " + StringUtils.join(attributeIndex, ',') + " -T -I -L");
            stringToWordVector.setOptions(options);
            
            WordTokenizer wordTokenizer = new WordTokenizer();
            String delimiters = " \r\t\n.,;:\'\"()?!-><#$\\%&*+/@^_=[]{}|`~0123456789 ”�–“•";
            wordTokenizer.setDelimiters(delimiters);
            stringToWordVector.setTokenizer(wordTokenizer);
            
            stringToWordVector.setInputFormat(instances);
            result = Filter.useFilter(instances, stringToWordVector);
        } catch (Exception ex) {
            Logger.getLogger(FilterManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    /**
     * Menerapkan filter StringToWordVector (secara batch) pada Instances train dan test, tepatnya pada atribut ke-attributeIndex.
     * @param train Instances yang difilter dengan filter StringToWordVector dan menjadi acuan format masukan.
     * @param test Instances yang difilter dengan filter StringToWordVector.
     * @param attributeIndex Nomor atribut diterapkannya filter StringToWordVector.
     * @return ArrayList Instances berisi hasil penerapan filter StringToWordVector (secara batch) pada Instances train dan test, tepatnya pada atribut ke-attributeIndex.
     */
    public static ArrayList<Instances> BatchStringToWordVector(Instances train, Instances test, ArrayList<Integer> attributeIndex) {
        ArrayList<Instances> result = new ArrayList<>();
        
        try {
            StringToWordVector stringToWordVector = new StringToWordVector();
            String[] options = weka.core.Utils.splitOptions("-R " + StringUtils.join(attributeIndex, ',') + " -T -I -L");
            stringToWordVector.setOptions(options);
            
            WordTokenizer wordTokenizer = new WordTokenizer();
            String delimiters = " \r\t\n.,;:\'\"()?!-><#$\\%&*+/@^_=[]{}|`~0123456789 ”�–“•";
            wordTokenizer.setDelimiters(delimiters);
            stringToWordVector.setTokenizer(wordTokenizer);
            
            stringToWordVector.setInputFormat(train);
            result.add(Filter.useFilter(train, stringToWordVector));
            result.add(Filter.useFilter(test, stringToWordVector));
        } catch (Exception ex) {
            Logger.getLogger(FilterManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
}
