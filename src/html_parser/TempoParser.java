/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package html_parser;

import crawler.HTMLParser;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Ahmad
 */
public class TempoParser implements HTMLParser {
    
    String content = "";
    String image = "";
        
    @Override
    public String getTextContent() {
        return content;
    }

    @Override
    public String getImage() {
        return image;
    }

    @Override
    public boolean parse(String url) {
        content = "";
        try {
            Document doc = Jsoup.connect(url)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get();
            
            // Extract Content
            Element artikel = doc.select(".artikel").first();
            content += ((Element)artikel.child(3)).text();
            if(content.lastIndexOf("Terpopuler") != -1)
                content = content.substring(0, content.lastIndexOf("Terpopuler"));
            else if(content.lastIndexOf("terpopuler") != -1)
                content = content.substring(0, content.lastIndexOf("terpopuler"));
            
            
            /*Elements ps_artikel = artikel.select("p");
            for(int i=0; i < ps_artikel.size(); i++) {
                Element p_artikel = ps_artikel.get(i);
                content += p_artikel.text() + "\n";
            }*/
            
            // Extract Image URL
            Element foto = doc.select(".foto-hl-besar").first();
            if(foto != null) {
                Element img = foto.select("img").first();
                
                if(img != null)
                    image = img.attr("src");
            }
            return true;
        } catch (IOException ex) {
            Logger.getLogger(TempoParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
}
