/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package informationextraction;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MLK
 * Membaca corpus 5W1H dari Pandu yang sudah dilengkapi label 5W1H per dokumen
 */
public class ReadCorpus {
    
    	public ArrayList<NewsArticle> proses (String namaFileInput) {
            File f = new File(namaFileInput);
            try {
                ArrayList<NewsArticle> corpus=new ArrayList<>();
                Scanner scLine = new Scanner(f);  //baca per baris
                String judul;
                String konten;
                String temp;
                int cc=1;
                while (scLine.hasNextLine()) {
                    //System.out.println(cc);
                    judul = scLine.nextLine();                                    
                    konten = scLine.nextLine();
                    //System.out.println(baris);
                    
                    corpus.add(new NewsArticle(judul, konten, true));
                    cc++;
                    //System.out.println();
                }
                scLine.close();
                //System.out.println("Selesai");
                return corpus;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ReadCorpus.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }

       public static void main(String[] args) {
           ReadCorpus rc=new ReadCorpus();
           rc.proses("I:\\Riset\\2014\\news_aggregator\\5W1H\\log\\kelompok.log");           
       } 
        
}