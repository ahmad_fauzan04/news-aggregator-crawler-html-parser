/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package informationextraction;

import IndonesianNLP.IndonesianNETagger;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author MLK
 */
public class Sentence {
    String text;
    ArrayList<String> NEList;   //NamedEntity
    ArrayList<String> token;
    ArrayList<String> tokenKind;
    ArrayList<String> cf;       //ContextualFeature
    ArrayList<String> mf;       //MorphologicalFeature
    ArrayList<String> posf;     //POSFeature
    ArrayList<String> Label;
    ArrayList<Boolean> time;

    public Sentence(String stc) {
        text=stc;
        if (text.split("[ ,]").length>1) {
            IndonesianNETagger inner = new IndonesianNETagger();        
            NEList = inner.extractNamedEntity(stc);
            token= inner.getToken();
            System.out.println(token.size());
            tokenKind= inner.getTokenKind();
            cf= inner.getContextualFeature();
            mf= inner.getMorphologicalFeature();
            posf= inner.getPOSFeature();
            time=extractTimeFeatures();
        }
        else {
            NEList=new ArrayList<>();
            NEList.add("OTHER");
            token=new ArrayList<>();
            token.add(text);
            tokenKind=new ArrayList<>();
            tokenKind.add("WORD");
            cf=new ArrayList<>();
            cf.add("_NULL_");
            mf=new ArrayList<>();
            mf.add("_NULL_");
            posf=new ArrayList<>();
            posf.add("_NULL_");
            time=new ArrayList<>();
            time.add(false);
        }
    }
    
    public ArrayList<Boolean> extractTimeFeatures() {
        //i.s: token sudah terisi token dari text
        //f.s: time berisi apakah
        ArrayList<Boolean> result=new ArrayList<>();
        //System.out.println("Sentence: "+text);
        boolean isTimePattern=false;
        for (int i=0;i<token.size();i++) {
            if (!isTimePattern)
                isTimePattern=cf.get(i).equals("DAY");
            else {
                //timePattern:
                //(cf=DAY) comma (mf=Digit) (cf=MONTH) (mf=Digit)
                //(cf=DAY)
                //(cf=DAY) '(' (mf=DigitSlash) ')'
                //i <> 0
//                            isTimePattern=(currWord.equals("'comma'") && befcf.equals("DAY")) || 
//                                          currWord.equals("'('") || 
//                                          currWord.equals("')'") || currcf.equals("MONTH") ||
//                                          ((currmf.equals("Digit")||currmf.equals("Numeric")) && (befWord.equals("'comma'") || befcf.equals("MONTH"))) ||
//                                          (currmf.equals("DigitSlash") && befWord.equals("'('"));
                String currWord=token.get(i);
                String befWord=token.get(i-1);
                String currmf=mf.get(i);
                String befcf=cf.get(i-1);
                isTimePattern=(currWord.equals("comma") && befcf.equals("DAY")) || 
                               currWord.equals("(") || 
                               currWord.equals(")") || cf.get(i).equals("MONTH") ||
                                ((currmf.equals("Digit")||currmf.equals("Numeric")) && (befWord.equals("comma") || befcf.equals("MONTH"))) ||
                                (currmf.equals("DigitSlash") && befWord.equals("("));
            }
            result.add(isTimePattern);                
        }        
        return result;
    }
    
    public void print() {
        //i.s: NEList, token, tokenKind,cf,mf,posf sudah terdefinisi
        //f.s: semua informasi per token ditampilkan ke layar
        
        System.out.println("Sentence: "+text);
        for (int i=0;i<token.size();i++) {
            System.out.print(token.get(i)+" - "+tokenKind.get(i)+" - "+cf.get(i)+" - "+mf.get(i)+" - "+posf.get(i)+" - "+NEList.get(i));
            if (Label!=null) System.out.println(" - "+Label.get(i));     
            else System.out.println();
        }
    }
    
    public static void main(String[] args) {    
        Sentence stc=new Sentence("Jakarta,Indonesia");
        stc.print();
    }
}
