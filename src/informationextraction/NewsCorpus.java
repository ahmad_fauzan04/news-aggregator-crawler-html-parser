/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package informationextraction;

import database.DatabaseHelper;
import database.DatabaseManager;
import java.io.PrintStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import structure.News;

/**
 *
 * @author MLK
 */
public class NewsCorpus {

    ArrayList<NewsArticle> corpus;
    
    public NewsCorpus(String fileName) {
        //corpus diload dari teks artikel yang terkumpul dalam satu file anotasi dari Pandu
        ReadCorpus rc=new ReadCorpus();
        corpus = rc.proses(fileName);
    }
    
    public NewsCorpus(boolean training) {
        //corpus diload dari teks artikel yang ada di db
        try {
            DatabaseHelper.Connect();
            String sql = "SELECT id_artikel, judul,full_text FROM artikel limit 101,1500";
            String parameters[] = new String[]{};
            ResultSet results = DatabaseHelper.executeQuery(sql,parameters);
            
            corpus=new ArrayList<>();
            while(results.next()) {
                NewsArticle article=new NewsArticle(results.getString("judul"), results.getString("full_text"), training);
                article.id=results.getInt("id_artikel");
                corpus.add(article);
            }
            DatabaseHelper.Disconnect();
            
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            DatabaseHelper.Disconnect();
        }
    }
    
    public void process() {
    //f.s: semua kalimat sudah ditokenisasi, ditentukan pos tags, NE tags, dst. Token sudah berlabel.
        for (int i=0;i<corpus.size();i++) {
            NewsArticle article=corpus.get(i);
            article.process();
            article.print();
            //System.out.println();
        }
    }

    public ArrayList<String> getTagSet() {
        ArrayList<String> result=new ArrayList<>();
            for (int i=0;i<corpus.size();i++) {
                NewsArticle article=corpus.get(i);
                for (int j=0;j<article.sentences.size();j++) {
                    Sentence stc=article.sentences.get(j);
                    for (int k=0;k<stc.token.size();k++) {
                        if (!result.contains(stc.posf.get(k)))
                            result.add(stc.posf.get(k));
                    }
                }
            }
        return result;
    }
    
    public void getIEFeatures() {
    //i.s: process() sudah dilakukan
    //f.s: menulis instance per token ke layar
        
            for (int i=0;i<corpus.size();i++) {
                String curr;
                NewsArticle article=corpus.get(i);
                for (int j=0;j<article.sentences.size();j++) {
                    Sentence stc=article.sentences.get(j);
                    for (int k=0;k<stc.token.size();k++) {
                        curr=stc.Label.get(k)+","+stc.token.get(k)+","+stc.tokenKind.get(k)+","+stc.posf.get(k)+","+stc.cf.get(k)+","+stc.mf.get(k)+","+stc.NEList.get(k);
                        System.out.println(curr);                
                    }
                }
            }               
    }
    
    public void writeIEDataset(String fileName) {
    //i.s: process() sudah dilakukan
    //f.s: menulis instance per token ke fileName 
        
        try {
            PrintStream writer=new PrintStream(fileName);
            writer.println("@relation IE5W1H");
            writer.println("@attribute label {beg_who,in_who,beg_what,in_what,beg_when,in_when,beg_where,in_where,beg_why,in_why,beg_how,in_how,other}");
            writer.println("@attribute currentWord string");
            writer.println("@attribute currentKind {PUNCT,NUM,WORD}");
            writer.println("@attribute currentTag {CC,CDC,CDI,CDO,CDP,CP,DT,Ekonomi,FW,MA,MTs,Mts,MI,IN,JJ,MD,NEG,NN,NNG,NNP,OP,PRL,PRN,PRP,PUNCT,RB,RP,SC,SYM,UH,VBI,VBT,WP,_NULL_}");
            writer.println("@attribute currentContextF {PPRE,PMID,PSUF,PTIT,POLP,OPRE,OSUF,OCON,OPOS,LPRE,LSUF,LLDR,LOPP,COUNTRY,DAY,MONTH,DPRE,QPRE,QSUF,_NULL_}");
            writer.println("@attribute currentMorphF {NumStr,TitleCase,UpperCase,LowerCase,MixedCase,Digit,CharDigit,DigitSlash,MoneyFormat,Numeric,TimeFormat,DateFormat,_NULL_}");
            writer.println("@attribute currentNETag {PERSON-B,PERSON-I,ORGANIZATION-B,ORGANIZATION-I,LOCATION-B,LOCATION-I,DATETIME-B,DATETIME-I,QUANTITY-I,QUANTITY-B,POSITION-B,POSITION-I,OTHER}");
            writer.println("@attribute bef1Label {start,beg_who,in_who,beg_what,in_what,beg_when,in_when,beg_where,in_where,beg_why,in_why,beg_how,in_how,other}");
            writer.println("@attribute bef1Word string");
            writer.println("@attribute bef1Kind {PUNCT,NUM,WORD,_NULL_}");
            writer.println("@attribute bef1Tag {CC,CDC,CDI,CDO,CDP,CP,DT,Ekonomi,FW,MA,MTs,Mts,MI,IN,JJ,MD,NEG,NN,NNG,NNP,OP,PRL,PRN,PRP,PUNCT,RB,RP,SC,SYM,UH,VBI,VBT,WP,_NULL_}");
            writer.println("@attribute bef1ContextF {PPRE,PMID,PSUF,PTIT,POLP,OPRE,OSUF,OCON,OPOS,LPRE,LSUF,LLDR,LOPP,COUNTRY,DAY,MONTH,DPRE,QPRE,QSUF,_NULL_}");
            writer.println("@attribute bef1MorphF {NumStr,TitleCase,UpperCase,LowerCase,MixedCase,Digit,CharDigit,DigitSlash,MoneyFormat,Numeric,TimeFormat,DateFormat,_NULL_}");
            writer.println("@attribute bef1NETag {PERSON-B,PERSON-I,ORGANIZATION-B,ORGANIZATION-I,LOCATION-B,LOCATION-I,DATETIME-B,DATETIME-I,QUANTITY-I,QUANTITY-B,POSITION-B,POSITION-I,OTHER}");
            writer.println("@attribute bef2Label {start,beg_who,in_who,beg_what,in_what,beg_when,in_when,beg_where,in_where,beg_why,in_why,beg_how,in_how,other}");
            writer.println("@attribute bef2Word string");
            writer.println("@attribute bef2Kind {PUNCT,NUM,WORD,_NULL_}");
            writer.println("@attribute bef2Tag {CC,CDC,CDI,CDO,CDP,CP,DT,Ekonomi,FW,MA,MTs,Mts,MI,IN,JJ,MD,NEG,NN,NNG,NNP,OP,PRL,PRN,PRP,PUNCT,RB,RP,SC,SYM,UH,VBI,VBT,WP,_NULL_}");
            writer.println("@attribute bef2ContextF {PPRE,PMID,PSUF,PTIT,POLP,OPRE,OSUF,OCON,OPOS,LPRE,LSUF,LLDR,LOPP,COUNTRY,DAY,MONTH,DPRE,QPRE,QSUF,_NULL_}");
            writer.println("@attribute bef2MorphF {NumStr,TitleCase,UpperCase,LowerCase,MixedCase,Digit,CharDigit,DigitSlash,MoneyFormat,Numeric,TimeFormat,DateFormat,_NULL_}");
            writer.println("@attribute bef2NETag {PERSON-B,PERSON-I,ORGANIZATION-B,ORGANIZATION-I,LOCATION-B,LOCATION-I,DATETIME-B,DATETIME-I,QUANTITY-I,QUANTITY-B,POSITION-B,POSITION-I,OTHER}");
            writer.println("@attribute isHeader {true,false}");
            writer.println("@attribute isTimePattern {true,false}");
            writer.println("@attribute isEOS {true,false}"); //end of sentence
            writer.println("@attribute isTokenJudul {true,false}"); //token judul bukan
            writer.println("@data");
            
            for (int i=0;i<corpus.size();i++) {
                String bef1="start,'<start>',_NULL_,_NULL_,_NULL_,_NULL_,OTHER";
                String bef2="start,'<start>',_NULL_,_NULL_,_NULL_,_NULL_,OTHER";
                String curr;
                String currcf, currmf, currWord;
                boolean isTimePattern=false;
                NewsArticle article=corpus.get(i);
                for (int j=0;j<article.sentences.size();j++) {
                    Sentence stc=article.sentences.get(j);
                    for (int k=0;k<stc.token.size();k++) {
                        currWord=stc.token.get(k);//.replace("'","");
                        currcf=stc.cf.get(k);
                        currmf=stc.mf.get(k);
                        isTimePattern=stc.time.get(k);
                        curr=stc.Label.get(k)+","+currWord+","+stc.tokenKind.get(k)+","+stc.posf.get(k)+","+currcf+","+currmf+","+stc.NEList.get(k);
                        int idx = article.arrTokenJudul.indexOf(currWord.replaceAll("'","").toLowerCase());
                        writer.println(curr+","+bef1+","+bef2+","+(j==0)+","+isTimePattern+","+(k==stc.token.size()-1)+","+(idx!=-1));
                        System.out.println(curr+","+bef1+","+bef2+","+(j==0)+","+isTimePattern+","+(k==stc.token.size()-1)+","+(idx!=-1));
                        bef2=bef1;
                        bef1=curr;
                    }
                }
            }
        }
       catch (Exception exp) {
            System.out.println(exp);
       }
        
                        //writer.print();
//                        if (bef1.equals("") && bef2.equals("")) {//kata pertama artikel
//                            writer.print(",'<start>',_NULL_,_NULL_,_NULL_,_NULL_,_NULL_,OTHER,other,'<start>',_NULL_,_NULL_,_NULL_,_NULL_,_NULL_,OTHER,other");                            
//                        }
//                        else if (k==1) {
//                            writer.print(","+stc.token.get(k-1)+","+stc.tokenKind.get(k-1)+","+stc.posf.get(k-1)+","+stc.cf.get(k-1)+","+stc.mf.get(k-1)+","+stc.NEList.get(k-1)+","+stc.Label.get(k-1)+",'<start>',_NULL_,_NULL_,_NULL_,_NULL_,_NULL_,OTHER,other");                            
//                        }
//                        else //k>=2
//                            writer.print(","+stc.token.get(k-1)+","+stc.tokenKind.get(k-1)+","+stc.posf.get(k-1)+","+stc.Label.get(k-1)+","+stc.token.get(k-1)+","+stc.tokenKind.get(k-2)+","+stc.posf.get(k-2)+","+stc.Label.get(k-2));                            
//                        //System.out.println(token.get(i)+" - "+tokenKind.get(i)+" - "+cf.get(i)+" - "+mf.get(i)+" - "+posf.get(i)+" - "+NEList.get(i)+" - "+Label.get(i));                
        
    }
    
        
    public static void main(String[] args) {
        NewsCorpus nc=new NewsCorpus("I:\\Riset\\2014\\news_aggregator\\5W1H\\log\\kelompok.log");
        nc.process();                
        //nc.writeIEDataset("I:\\Riset\\2014\\news_aggregator\\5W1H\\dataset30_19112014.arff");
        //nc.getIEFeatures();
        
    }
            
}
